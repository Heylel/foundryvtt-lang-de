## Installation

To install, follow these instructions:

1.  Inside FoundryVTT, select the Add-on Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: https://gitlab.com/Heylel/foundryvtt-lang-de/raw/master/de-DE/module.json
3.  Click Install and wait for installation to complete.

-----------------------------------------------------------------------

Zur Installation folgende Anweisungen befolgen:

1.  Innerhalb von FoundryVTT den "Add-on modules" tab im Hauptmenü "Configuration and Setup" öffnen.
2.  Den "Install Module" Button klicken und folgende URL eingeben: https://gitlab.com/Heylel/foundryvtt-lang-de/raw/master/de-DE/module.json
3.  Installieren klicken und warten, bis die Installation abgeschlossen ist.
